#!/usr/bin/perl
use strict;
use warnings;

use Test::Simple tests => 1;

BEGIN
{
    require Latin; Latin->import();
    ok(1, 'Latin loads successfully');
}
