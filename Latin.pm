package Latin;
use 5.022;
use experimental qw(signatures);
use warnings;
use strict;

# Encoding
use constant ENC => ':encoding(UTF-8)';
use utf8;
use open ':std' => ENC;
use Encode qw(encode decode);
use Unicode::Normalize qw(NFD NFC); # We use NFD coming in and NFC going out

use Carp qw(carp croak confess cluck);
use List::Util qw(pairs shuffle);

use Data::Dump; # DEBUG
use constant DEBUG => 1;

use Latin::Verb;
use Latin::Noun;
# use Latin::Adjective;

my @verbs = Verb::_load_verbs();
my @nouns = Noun::_load_nouns();
dd @verbs;
dd @nouns;
exit;
1; # End of package
