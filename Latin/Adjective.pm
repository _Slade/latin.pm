package Adjective
{
    use experimental qw(signatures);
    use constant DEBUG => 1;
    use constant {
        MASC,
        FEM,
        NEUTER,
        STEM,
    };
    my ($stem, $m, $f, $n);
    sub new
    {
        die unless @_ == 5;
        bless [@_], __PACKAGE__;
    }

    sub stem($self) { $self->[1] }
    sub m($self)    { $self->[2] }
    sub f($self)    { $self->[3] }
    sub n($self)    { $self->[4] }

    sub matches_any($self, $word)
    {
        state $pattern = sprintf q((?-i)\A%s(?:%s)\z), 
              $self->stem, 
              (join '|', @{$self}[1 .. 3]);
        say $pattern if DEBUG;
        $word =~ m/$pattern/;
    }
    
    use overload q("") => sub {
        my $self = shift;
        sprintf '%s%s -%s -%s', $self->stem, $self->m, $self->f, $self->n;
    };
    1;
}
