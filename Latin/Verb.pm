package Verb;
use strict;
use warnings;
use feature qw(unicode_strings);
use experimental qw(signatures);
use constant ENC => ':encoding(UTF-8)';
use utf8;
use open ':std' => ENC;
binmode   DATA  => ENC;
use Unicode::Normalize qw(NFD NFC);

use Carp qw(carp croak confess cluck);
use Text::CSV;

use Data::Dump qw(dump); # DEBUG
use constant DEBUG => 1;

# The four principle parts of a verb are:
#   First person singular present active indicative
#   Present active infinitive
#   First person singular perfect active indicative
#   Perfect passive participle

# Principle parts
use constant {
    FIRST_PP  => 0,
    SECOND_PP => 1,
    THIRD_PP  => 2,
    FOURTH_PP => 3,
};

# Attribute fields
use constant {
    PPS  => 0,
    DEFS => 1,
};

# Conjugations
use constant {
    FIRST   => 0,
    SECOND  => 1,
    THIRD   => 2,
    THIRD_I => 3,
    FOURTH  => 4,
};

our %infinitive_endings = (
    NFD('āre') => FIRST,
    NFD('ēre') => SECOND,
    NFD('ere') => THIRD,
    NFD('rre') => THIRD, # auferre, disferre (irregular)
    NFD('īre') => FOURTH
);

sub new($self, $conjugation, @rest)
{
    bless [
        $conjugation,
        [@rest[0 .. 3]],      # Principal parts
        [@rest[4 .. $#rest]], # Definitions
    ], $self;
}

sub conjugate($self, @conj_params)
{
    ...
}

sub _load_verbs
{
    my $conjugation;
    my $csv = Text::CSV->new({ binary => 1 }) or die Text::CSV->error_diag();
    my @verbs;
    while (my $line = $csv->getline(\*DATA))
    {
        next unless @$line >= 4;
        my $inf_end = substr $line->[1], -3;
        $conjugation = $infinitive_endings{ NFD($inf_end) };
        die 'Could not get conjugation of ' . dump($line) . " $inf_end"
            unless defined $conjugation;

        # Handle I-stem 3rd conjugation verbs
        if ($conjugation == THIRD and substr($line->[0], -2) eq 'iō')
        {
            $conjugation = THIRD_I
        }
        push @verbs, Verb->new($conjugation, @$line);
    }
    return \@verbs;
}

1;

__DATA__
ambulō, ambulāre, ambulāvī, ambulātum, walk
amō, amāre, amāvī, amātus, love
cōgitō, cōgitāre, cōgitāvī, cōgitus, think
donō, donāre, donāvī, donātus, give, present, reward
dō, dāre, dedī, datus, give, grant
errō, errāre, errāvī, errātus, wander, err
laborō, laborāre, laborāvī, laborātus, work, suffer
laudō, laudāre, laudāvī, laudātus, praise
līberō, līberāre, līberāvī, līberātus, free
monstrō, monstrāre, monstrāvī, monstrātus, show, point, out
optō, optāre, optāvī, optātus, desire
pugnō, pugnāre, pugnāvī, pugnātus, fight
superō, superāre, superāvī, superātus, overcome, conquer, surpass
vocō, vocāre, vocāvī, vocātus, call, summon, name

careō, carēre, caruī, caritūrus, lack, be without, be free (from) (+abl.)
debeō, debēre, dēbuī, debitus, owe, ought
habeō, habēre, habuī, habitus, have, hold, consider
iubeō, iubēre, iussī, iussus, order
maneō, manēre, mānsī, mānsūrus, remain, stay, await (tr.)
moveō, movēre, mōvī, mōtus, set in motion, stir (up), move
respondeō, respondēre, respondī, respōnsus, answer respond
teneō, tenēre, tenuī, tentus, hold, grasp, keep, possess, occupy
terreō, terrēre, terruī, territus, terrify, frighten
timeō, timēre, timuī, n/a, fear, be afraid (of)
videō, vidēre, vīdī, vīsus, see

accipiō, accipere, accēpī, acceptus, receive, accept, hear (of), learn (of)
accēdō, accēdere, accessī, accessum, got to, come to, approach
agō, agere, egī, āctus, drive, do, spend, conduct
auferō, auferre, abstulī, ablātus, carry array, take away, remove
canō, canere, cecinī, cantus, sing (of)
capiō, capere, cēpī, captus, take (up), capture, win
cupiō, cupere, cupiī, cupītus, desire, long for, want
cēdō, cēdere, cessī, cessum, go, move, yield, withdraw
differō, differre, distulī, dīlātus, differ (intr.), be different, carry away
discēdō, discēdere, discessī, discessum, go away, depart
dīcō, dīcere, dīxī, dictus, say, speak, tell, dictate
dūcō, dūcere, dūxī, ductus, lead, consider
faciō, facere, fēcī, factus, make, do
fugiō, fugere, fūgī, fugitūrus, flee
gerō, gerere, gessī, gestus, bear, manage, conduct, perform
intellegō, intellegere, intellēxī, intellēctus, understand
interficiō, interficere, interfēcī, interfectus, kill
legō, legere, lēgī, lēctus, choose, read
mittō, mittere, mīsī, missus, send
perificiō, perficere, perfēcī, perfectus, complete, accomplish
petō, petere, petiī, petītus, ask (for), seek, attack
ponō, pōnere, posuī, positus, put, place, set aside
regō, regere, rēxī, rēctus, rule, control
scrībō, scrībere, scrīpsī, scrīptus, write
trādō, trādere, trādidī, trāditus, hand over, surrender, hand down
vincō, vincere, vīcī, victus, conquer, overcome, win (intr.)
vīvō, vīvere, vīxī, vīctūrus, live, be alive

abeō, abīre, abiī, abitum, go away
audiō, audīre, audīvī, audītus, hear, listen (to)
sentiō, sentīre, sēnsī, sēnsus, perceive, feel
veniō, venīre, venī, ventum, come, arrive
abeō, abīre, abiī, abitum, go away
redeō, redīre, rediī, reditum, go back, return
