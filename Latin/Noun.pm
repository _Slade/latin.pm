=pod

=encoding utf8

=head1 NAME

Latin::Noun

=head1 SYNOPSIS

    my $noun = Latin::Noun->new($decl, $gender, $pp1, $pp2, @defs);
    say 'The accusative plural of '
        . $noun->canonical_string()
        . 'is ' $noun->inflect(...); # Interface TBD

=head1 DESCRIPTION

This module deals with Latin nouns. It uses an OO interface to provide Noun
objects which can be declined (inflected) and otherwise manipulated.

This module also contains several packages containing constants.

=cut

package Noun;
use 5.022;
use strict;
use warnings;
use feature qw(unicode_strings);
use experimental qw(signatures);
use constant ENC => ':encoding(UTF-8)';
use utf8;
use open ':std' => ENC;
binmode   DATA  => ENC;
use Unicode::Normalize qw(NFD NFC);

use Carp qw(carp croak confess cluck);
use Text::CSV;

# use Data::Dump; # DEBUG
use constant DEBUG => 1;

# Internal
use constant {
    _DECLENSION => 0,
    _PPARTS     => 1, # Nominative, Genitive singular forms
    _GENDER     => 2,
    _DEFS       => 3
};

=head2 package Declension

Contains the constants C<FIRST>, C<SECOND>, C<THIRD>, C<THIRD_I>, C<FOURTH>, and
C<FIFTH>, which are used in the Noun constructor.

=cut
package Declension
{
    use constant {
        FIRST   => 0,
        SECOND  => 1,
        THIRD   => 2,
        THIRD_I => 3,
        FOURTH  => 4,
        FIFTH   => 5
    };
}

package Gender
{
    use constant {
        MALE   => 0,
        FEMALE => 1,
        NEUTER => 2,
        EITHER => 3,
    };

    my %genstrs = (
        'M'   => MALE,
        'F'   => FEMALE,
        'N'   => NEUTER,
        'M/F' => EITHER,
    );

    sub to_constant($s = '')
    {
        if ($s and exists $genstrs{$s}) { return $genstrs{$s} }
        else { Carp::croak("Invalid string '$s'") }
    }
}

my %decl_endings = (
    NFD('ae')   => Declension::FIRST,
    NFD('ārum') => Declension::FIRST,
    NFD('ī')    => Declension::SECOND,
    NFD('ōrum') => Declension::SECOND,
    NFD('is')   => Declension::THIRD,
    NFD('um')   => Declension::THIRD,
    NFD('ium')  => Declension::THIRD_I,
    NFD('ūs')   => Declension::FOURTH,
    NFD('uum')  => Declension::FOURTH,
    NFD('eī')   => Declension::FIFTH,
    NFD('ēī')   => Declension::FIFTH,
    NFD('ērum') => Declension::FIFTH,
);

=head2 Latin::Noun->new($decl, $gender, $pp1, $pp2, @defs)

Constructs a new Latin::Noun object using the parameters:

=over 4

=item $decl

The declension of this noun. There are ten sets of noun case endings in Latin
comprised of five declensions. The third declension has I-stem variants and the
second, third, third-I, and fourth declensions have neuter endings in addition
to the usual gendered endings. The declension of a noun can be identified by its
ending in the genitive case.

$decl should be one of the package constants C<FIRST>, C<SECOND>, C<THIRD>,
C<THIRD_I>, C<FOURTH>, or C<FIFTH>.

=item $gender

This parameter should be one of the package constants C<Gender::MALE>,
C<Gender::FEMALE>, C<Gender::BOTH>, C<Gender::NEUTER>.

Latin nouns may be either masculine, feminine, both, or neither. The declensions
correspond to genders as follows:

=over 4

=item First

Almost entirely feminine, with a few exceptions (I<poēta>, I<incola>,
I<agricola>, I<nauta>, ...).

=item Second

Almost entirely masculine (nominative ending I<-us>, I<-er>, ...) or neuter
(nominative ending I<-um>).

=item Third and its I-stem variants

An almost equal mix of masculine, feminine, or neuter. Contains a few rare words
that are both genders (I<cīvis>, I<nēmō>, ...).

=item Fourth

Almost always masculine.

=item Fifth

Almost always feminine.

=back

=item $pp1

The nominative singular form of the noun.

TODO: What to do about nouns that are always plural.

=item $pp2

The genitive singular form of the noun.

=item @defs

The definitions of this noun.

=back

=cut
sub new($self, $decl, $gender, @rest)
{
    @rest < 3 and die
        'A noun requires two principal parts and at least one definition';
    my ($pp1, $pp2) = (shift @rest, shift @rest);
    bless [
        $decl,
        $gender,
        $pp1,
        $pp2,
        [ @rest ]
    ], $self;
}

=head2 inflect

Declines/inflects a noun to a specific case and number.

=cut
sub inflect($self, @infl_params)
{
    ...
}

=head2 _load_nouns

An internal function used to parse the set of known nouns. Returns an arrayref
containing these nouns.

=cut
sub _load_nouns
{
    my $decl;
    my $csv = Text::CSV->new({ binary => 1, allow_whitespace => 1 });
    my @nouns;
    while (my $line = readline DATA)
    {
        chomp $line;
        next if $line =~ m/\s*#/ or not $line;
        my $status = $csv->parse($line);
        my @cols = $csv->fields();

        croak("Not enough fields") if @cols < 4;

        # Layout: pp1, pp2, -ium (3rd-I only), gender, definitions
        my ($pp1, $pp2, $gender, $decl, @defs) = splice @cols, 0, 2;

        if ($cols[0] eq '-ium')
        {
            shift; # Bump off -ium
            $decl   = Declension::THIRD_I;
        }
        elsif ($pp2 =~ m{
            (ae|ārum|(?<![eē])ī|ōrum|is|um|ium|ūs|uum|eī|ēī|ērum)\z
        }x)
        {
            my $m = NFD($1); # Normalize match
            croak("Bad ending $1") if DEBUG and not exists $decl_endings{$m};
            $decl = $decl_endings{$m};
        }
        else { croak("Couldn't detect declension in '$pp2'") }

        $gender = Gender::to_constant(shift);
        # Removed pp1, pp2, -ium, and gender at this point

        push @nouns, Noun->new($decl, $gender, $pp1, $pp2, @cols);
    }
    return \@nouns;
}

1;

__DATA__
Athēnae, Athēnārum, F, Athens (pl.)
āra, ārae, F, altar
agricola, agricolae, M, farmer
amīcitia, amīcitiae, F, friendship
anima, animae, F, life force, soul
cōpia, cōpiae, F, abundance, troops (pl.), forces (pl.)
cūra, cūrae, F, care, concern, anxiety
causa, causae, F, reason, cause, case
dīligentia, dīligentiae, F, diligence
dea, deae, F, goddess
fāmā, fāmae, F, report
fēmina, fēminae, F, woman, wife
fīlia, fīliae, F, daughter
fortūna, fortūnae, F, fortune, chance
fuga, fugae, F, flight
glōria, glōriae, F, renown, glory
incola, incolae, M, inhabitant
inimīcitia, inimicītiae, F, enmity, hostility
īnsidiae, īnsidiārum, F, ambush (pl.), plot, treachery
īnsula, īnsulae, F, island
invidia, invidiae, F, envy, jealousy, ill-will, resentment
īra, īrae, F, anger, wrath
Italia, Italiae, F, Italy
mora, morae, F, delay
nātūra, nātūrae, F, nature
nauta, nautae, M, sailor
patria, patriae, F, homeland, country
pecūnia, pecūniae, F, money
poēta, poētae, M, poet
poena, poenae, F, punishment, penalty
prōvincia, prōvinciae, F, province
puella, puellae, F, girl
rēgīna, rēgīnae, F, queen
Rōma, Rōmae, F, Rome
sapientia, sapientiae, F, wisdom
sententia, sententiae, F, thought, feeling, opinion
terra, terrae, F, land, earth
umbra, umbrae, F, shadow, shade
vīta, vītae, F, life
via, viae, F, way, road, street, path

ager, agrī, M, field
altum, altī, N, deep sea, height
amīcus, amīcī, M, friend
animus, animī, M, (rational) soul, mind
annus, annī, M, year
arma, armōrum, N, arms (pl.), weapons
aurum, aurī, N, gold
auxilium, auxiliī, N, aid, help, auxiliary troops (pl.)
bellum, bellī, N, war
caelum, caelī, N, sky, heaven
cōnsilium, cōnsiliī, N, deliberation, plan, advice, judgment
deus, deī, M, god
dictum, dictī, N, word, saying
dominus, dominī, M, master, lord
domus, domī, F, house, home
dōnum, dōnī, N, gift
exsilium, exsiliī, N, exile
exilium, exiliī, N, exile
factum, factī, N, deed
ferrum, ferrī, N, iron, sword
forum, forī, N, public square, forum, marketplace
fātum, fātī, N, destiny, fate
fīlius, fīliī, M, son
gaudium, gaudiī, N, joy
gladius, gladiī, M, sword
imperium, imperiī, N, power, authority, command, empire
ingenium, ingeniī, N, nature, disposition, (natural) talent
inimīcus, inimīcī, M, (personal) enemy,
iussum, iussī, N, order, command
liber, librī, M, book
locus, locī, M, place
modus, modī, M, measure, limit, rhythm, meter, manner, way
oculus, oculī, M, eye
odium, odiī, N, hatred
oppidum, oppidī, N, town
perīculum, perīculī, N, danger
populus, populī, M, (the) people, populace
proelium, proeliī, N, battle
puer, puerī, M, boy
Rōmānī, Rōmānōrum, M, the Romans (pl.)
servus, servī, M, slave, servant
socius, sociī, M, ally, comrade
studium, studiī, N, zeal, enthusiasm, pursuit, study
templum, templī, N, temple
verbum, verbī, N, word
vir, virī, M, man, husband
vēlum, vēlī, N, sail

Carthāgō, Carthāginis, F, Carthage
amor, amōris, M, love
animal, animālis, -ium, N, animal
ars, artis, -ium, F, skill, art, guile, trick
carmen, carminis, N, song, poem
corpus, corporis, N, body
cīvis, cīvis, -ium, M/F, citizen
cīvitās, cīvitātis, F, state, citizenry, citizenship
frāter, frātris, M, brother
homō, hominis, M, human being, man
hostis, hostis, -ium, M, (public) enemy
iūs, iūris, N, right, law, judgment, court
lībertās, lībertātis, F, freedom
mare, maris, -ium, N, sea
moenia, moenium, N, (city) walls
mors, mortis, -ium, F, death
māter, mātris, F, mother
mēns, mentis, -ium, F, mind, intention, purpose, attitude
mīles, mīlitis, M, soldier
mūnus, mūneris, N, service, duty, gift
nox, noctis, -ium, F, night
pars, partis, -ium, F, part
pater, patris, M, father
pāx, pācis, F, peace, favor
rēx, rēgis, M, king
rūs, rūris, N, country(side)
servitūs, servitūtis, F, slavery
soror, sorōris, F, sister
tempus, temporis, N, time
timor, timōris, M, fear
urbs, urbis, -ium, F, city
virtūs, virtūtis, F, manliness, courage, excellence, virtue
vōx, vōcis, F, voice, word

cōnsulātus, cōnsulātūs, M, consulship
domus, domūs, F, house, home
exercitus, exercitūs, M, army
manus, manūs, F, hand, band, troop
mōtus, mōtūs, F, motion, movement, disturbance
senātus, senātūs, M, senate

aciēs, aciēī, F, sharp edge, keenness, battle line
diēs, diēī, M/F, day
fidēs, fidēī, F, faith, trust, trustworthiness, loyalty
rēs, reī, F, thing, property, matter, affair, situation
